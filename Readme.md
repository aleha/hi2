# Ordinal Classification in High Dimensions

Hierarchical twoing (hi2) is a scheme for
classification of high-dimensional data into ordered categories.

hi2 combines the power of well-understood binary
classification with ordinal response prediction.

This project contains an R package implementing the hi2 scheme.

## Installation

Best installed with the `devtools` package:

    library("devtools")
    install_git("git@gitlab.com:hi2/hi2.git")